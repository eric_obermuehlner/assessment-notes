# assessment-notes

Notes for the assessment of Java Developers

This document goes together with the project `assessment-calculator`.

## General Questions

- What do you think makes a good developer?

- What do you think makes a good manager?

- What do you think you would add to the team if you were to get the job?

- What would you expect from the team if you were to get the job?

- If you were to organise a work social night out, what would you plan?

- What do you like to do when you aren't coding?


# Developer Assessment

## Coding Questions

- What are `Collection`, `List`, `Set`, `Map`?
    - Follow up questions:
        - Typical implementations?
            - Answer: `ArrayList`, `HashSet`, `HashMap`, `TreeMap`
            - O(n) on different operations and implementations?
            - What are `hashCode()` and `equals()` and what relationsship do they have to collections?
            - Thread safety?
                - Answer: `ConcurrentHashMap`, `ConcurrentArrayList`, `Collections.synchronizedList()`, ...
            - What is a `ConcurrentModificationException` and how to get rid of it?
                - Answer: usually removing/adding elements to collection while iterating
	  
- What is Composition over Inheritance ?

- What is Dependency Injection ?

- What is immutability and what are its advantages and disadvantages?
    - Answer: Thread safety
    - Answer: Safe to pass as arguments
    - Answer: Tend to need many copies (memory, performance issues)
    - Answer: Difficult to construct if many parameters
    - Follow up questions:
        - Examples for immutable classes in Java?
            - Answer: `String`, `BigDecimal`
        - Solutions to mitigate the problem with difficult constructors?
            - Answer: Builder pattern to construct mutable and then turn immutable

- Good and bad about static utility methods?
    - Answer: Easy to use
    - Answer: Compact code (static imports)
    - Answer: Cannot be replaced with a different implementation (bad for testing)

- What is the class `BigDecimal`?
    - Follow up questions:
        - Discuss `new BigDecimal(0.1)`
            - Answer: Use `BigDecimal.valueOf(0.1)` instead
        - Discuss `b.equals(BigDecimal.ZERO)`
            - Answer: Use `b.compareTo(BigDecimal.ZERO) == 0` or `b.signum() == 0` instead

- Discuss checked versus unchecked exceptions.
    - Follow up questions:
        - Good to throw `RuntimeException`?
        - Other patterns of error handling?


## Notes to the `assessment-calculator`

The following issues are "hidden" in the project:

- Unit tests have no asserts
- Unit tests are incomplete

- `CalculatorApplication`
    - `main()` does not check args
    - `main()` passes args in wrong order
    - Field `calculator` is of type impl class
    - Field `calculator` is set twice
    - Field `calculator` is instantiated explicitely (dependency injection?)
  
- `NumberService`
    - `convertToDouble()` returns `Double` instead of `double`
    - does not specify what happens if the string is invalid
  
- `NumberServiceImpl`
    - consider `@Override` annotation on `convertToDouble()`
  
- `CalculatorService`
    - does not specify what happens if the arguments are invalid
    - Argument `String elements[]` should be `String[] elements`
    - Consider argument `String... elements`
  
- `CalculatorServiceImpl`
    - does not implement `CalculatorService`
    - Fields are public
    - Field `numberService` is of type impl class
    - Field `numberService` is instantiated explicitely (dependency injection?)
    - Undocument and unfinished feature "variables"
    - `setVar()`, `unsetVar()`, `getVar()` should be in interface `CalculatorService`
    - Field `variables` is of type impl class `HashMap` instead of `Map`
    - Field `variables` does not use generic types
    - `unsetVar()` is a stupid impl (loops over elements to remove element)
    - `getVar()` needs an unnecessary cast, because generic types are missing on `variables`
    - In `calculate()`:
        - `calculate()` has different signature than in interface
        - consider `@Override` annotation on `calculate()`
          (would have found that it does not implement the interface)
        - `"unset"` passes the wrong argument `element[0]`
        - `"unset"` misses return statement and falls through
        - uses `==` instead of `equals()`: `elements[1] == "-"`
        - conversion to String is unnecessary complex: `new String(new Double(v1 - v2).toString())`
        - `"+"` operation is in the else cases and hides wrong arguments
    - In `convertToDouble()`:
        - has undocumented feature `"pi"`
        - uses `==` instead of `equals()`: `string == "pi"`
        - consider implementing remainder of feature "variables" here

