# DevOps Assessment

- General
- Git
- Linux Administration
- Windows Administration
- Jenkins
- PostgreSQL Administration
- VMWare
- Certificates
- Docker

## General

- Q: What are important Operational aspects for running products/services?
  - A: Like 24/7 monitoring and uptime (for example on-call), Monitoring setup to detect abnormalities, Patch Management,  Backup process, Operational routines, Production first, …

- Q: What experience do you have with application uptime monitoring and performance monitoring. What tools are you familiar with?

- Q: What is your troubleshooting process / How do you analyze and solve issues?

- Q: How familiar are you with different operating systems? How would you assess your skills on different OS’s (scale 1 – 10)

- Q: What automation tools are you familiar with. (Ansibel, Puppet, Terraform, Powershell, …)

- Q: What is the role of configuration management?

- Q: Explain the term "Infrastructure as Code" (IaC) as it relates to configuration management.

- Q: How does continuous monitoring help you maintain the architecture of the system?

## Git

- Q: What is a Git rebase? When would you use it and when would you prefer a merge?

- Q: How would you revert a commit that has already been pushed to a remote repository?


## Linux Administration

- Q: Explain the usage and syntax of the chmod command. How would you give read, write, and execute permissions to a file for the owner, group, and others?

- Q: How do you troubleshoot high CPU utilization on a Linux server? Mention some command-line tools you would use.
     What if the application is a Java application?
   
- Q: Describe the purpose of the cron and systemd services in Linux.
     Provide an example of setting up a cron job.
     Provide an example of setting up a systemd service.
   

## Windows Administration

- Q: How do you add a user to a Windows Active Directory domain?   

- Q: Explain the purpose of the Windows Registry. How would you edit it using built-in tools?

- Q: Describe the difference between a Windows service and a regular application.

- Q: How do you configure Windows Firewall rules to allow specific network traffic?


## Jenkins

- Q: Explain the concept of Jenkins pipelines. How do they differ from traditional job configurations?
  - A: Jenkins pipelines are a way to define and automate your continuous integration and continuous delivery (CI/CD) workflows as code. (Pipeline DSL)

- Q: Describe the purpose of Jenkins agents (formerly slaves) and how they contribute to distributed builds.


## Network

- Q: What is the difference between TCP and UDP? In which scenarios would you use each protocol?

- Q: How does DNS work? Can you describe the process of resolving a domain name to an IP address?

- Q: What is a subnet mask, and how does it relate to IP addressing and routing?

- Q: Describe the purpose of a Load Balancer in a network architecture.


## PostgreSQL

- Q: What is a schema in PostgreSQL? How can multiple schemas be beneficial in a database?

- Q: How do you create a backup of a PostgreSQL database? What are the different methods available?
  - A: Use pg_dump to create a backup of a PostgreSQL database. Options like -Fc create a custom-format backup for more efficient restoration.
       To restore a database from a dump file, use pg_restore or psql with the dump file as input.
    
- Q: What is the significance of the VACUUM command in PostgreSQL, and when would you need to run it?
  - A: VACUUM reclaims space and optimizes table performance by cleaning up dead rows. Run it regularly to maintain database health.

- Q: Describe the process of setting up user roles and permissions in PostgreSQL to ensure security and access control.
  - A: Use CREATE ROLE to set up user roles and GRANT to assign permissions. Apply the principle of least privilege to ensure security.

- Q: What do you do when a PostgrSQL server does not start?
  - A: check: log files, configuration, ports, resources

## VPN


## VMWare


## Certificates

- Q: What is the purpose of an SSL/TLS certificate? How does it contribute to secure communication over the internet?

- Q: Describe the process of creating a Certificate Signing Request (CSR) and obtaining a certificate from a CA.

- Q: What is a self-signed certificate? What are the advantages and disadvantages of using self-signed certificates?

- Q: How do you configure a web server (e.g., Apache or Nginx) to use SSL/TLS certificates for secure connections?

- Q: Describe the difference between symmetric and asymmetric encryption. When would you use one over the other?


## Docker

- Q: Explain the difference between a Docker image and a Docker container.

- Q: How would you create a Docker image? What is the significance of a Dockerfile?

- Q: What is a multi-stage build in Docker, and why would you use it?

- Q: Describe the Docker networking modes: bridge, host, and overlay. When would you use each mode?

- Q: What is Kubernetes, and how does it relate to Docker? How does Kubernetes manage container orchestration?
